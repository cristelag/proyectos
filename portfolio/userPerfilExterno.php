<?php
session_start();
$usuario=$_GET['usuario'];


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png2" href="../imagenes/logo3.png" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">    <script src="https://kit.fontawesome.com/9e71a52480.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
  <script src="funciones.js"></script>



    <title>ICYOU - perfil @<?php echo $usuario?></title>
    <style>
    body{
        background-color: #f6f5f7;

    }
    .navbar-brand {
        font-family: 'Montserrat', sans-serif;
            font-size: 40px;
            font-weight: bold;

        }

        .navbar {
            background-color: black;
            text-transform: uppercase;

        }
      
        .heading{    
            padding: 10px 10px;
            border-radius: 2px;
            color: grey;
            margin-bottom:10px;
            text-transform: uppercase;
            font-size: 0.75rem;
            line-height: 1.85583232;
            -webkit-column-span:all;
            column-span:all;
            justify-content:center;
           

        }
        .heading span{
            margin-right: 10px;
        }
        .heading span:hover{
            color:black;
            cursor:pointer;
           
            
        }
        #galeria{
            text-align:center;
            margin: 1rem auto;
            width:100%;
            max-width:960px;
            column-count: 3;
            break-inside: avoid;
            page-break-inside: avoid;

        }
div .image img{
    margin:.2em;
}
.contenedor_fotoperfil{
    width: 13em;
    height:13em;
    overflow: hidden;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    -ms-border-radius: 100%;
    -o-border-radius: 100%;
    border-radius: 100%;
}
.userPic{
    width:100%;
}
.loginUser{
        margin-top:1em;
        font-weight:600;
            letter-spacing: 0.1rem;
        }
    </style>
</head>
<body onload="nobackbutton();">
<?php 
if(isset($logout)){
    session_destroy();
   
}

?>
<!--contenedor general-->
<div class="container-fluid">
 <!--navegación-->
 <nav class="navbar navbar-expand-lg navbar-dark  shadow fixed-top">
            <div class="container">
                <a class="navbar-brand" href="inicio.php">
                    <img src="../imagenes/logo3.png" width="55" height="55" class="d-inline-block align-top" alt=""> ICYOU
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="inicio.php">Inicio
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="explorePage.php">Explorar</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="userPerfil.php">Perfil   <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.php">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--fin navegación-->
        </nav>
        <!--fila para el usuario-->
    
<div class="row d-flex justify-content-center pt-5 mt-5">
<div class="contenedor_fotoperfil mt-5">
<?php 
  $conexion = mysqli_connect('localhost', 'admin', '', 'icyou');
  if (isset($_SESSION['usuLogin']) && isset($_SESSION['usutipo'])) {


      if ($_SESSION['usutipo'] == "fotografo") {
          $conexion = mysqli_connect('localhost', 'fotografo', '', 'icyou');
          if (mysqli_connect_errno()) {
              printf("Conexión fallida %s\n", mysqli_connect_error());
              exit();
          }
          $sql = "SELECT login,fotoPerfil FROM usuarios WHERE login='$usuario' ";
          $resultado = mysqli_query($conexion, $sql);
          if (mysqli_num_rows($resultado) > 0) {
              while ($registro = mysqli_fetch_row($resultado)) {
?>
<img class="userPic"  src="<?php echo  $registro[1]?>">
<?php
              }
            }
?>
</div>
</div>
<!--final fila-->

<div class="row justify-content-center">
<p class="loginUser">@<?php echo $usuario?></p>
</div>
<section class="py-5 mt-5">
            <div class="container">

                <hr>
                <div id="galeria" class="animate__animated animate__fadeInUp"><!--grid de imagenes de la bd-->
                <?php
              
                        $sql = "SELECT idFoto, idUser,ruta FROM fotos WHERE idUser='$usuario' ";
                        $resultado = mysqli_query($conexion, $sql);
                        if (mysqli_num_rows($resultado) > 0) {
                            while ($registro = mysqli_fetch_row($resultado)) {
                ?>
            <div class="image">
            <a href="fotoDetalle.php?idFoto=<?php echo $registro[0];?>"><img class="img-fluid"  width="350px" height="auto" src="<?php echo $registro[2]; ?>" /><a/>                            </div>



                <?php
                            }
                        }
                    }
                }
                mysqli_close($conexion);
                ?>
                
            </div>
            </div>
         
        </section>


</div>
<!--fin contenedor general-->
    
</body>
</html>