<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
<?php
$peli=$_GET['pel'];
?>

<!--contenedor general-->
<div class="container-fluid">
       <!--navegación-->
       <nav class="navbar navbar-expand-lg navbar-dark  shadow fixed-top">
            <div class="container">
                <a class="navbar-brand" href="inicio.php">
                    <img src="../imagenes/logo3.png" width="55" height="55" class="d-inline-block align-top" alt=""> ICYOU
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="inicio.php">Inicio
                               
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="explorePage.php">Explorar</a>
                            <span class="sr-only">(current)</span>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="userPerfil.php">Perfil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.php" >Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
         
        </nav>
           <!--fin navegación-->
           <!--etiquetas-->
           <div class=" py-5 mt-5" id="etiquetas">
             <span id="desc" class=" d-flex mb-3 mt-5 justify-content-center">¡sigue algunos hashtags para ver el contenido que te interesa!</span>
                 <div id="buscador" class="d-flex justify-content-center">
                    <form action="#" method="POST">
                        <input type="text" id="tags-input" required name="etiquetaIntro"> 
                        <button type="submit" class="btn" name="buscar"> <span class="material-icons">search</span></button>
                        <button class="follow" name="seguir">Seguir</button> |
                        <button class="follow" id="dseg" name="dseguir">Dejar de seguir</button>

                    </form>
                </div>
  <span name="error"></span>

           </div>
           <!-- fin etiquetas-->

        <!--contenido-->
    <section class="py-5 mt-5">
        <div class="container">
            <hr>
                <div id="galeria" class="animate__animated animate__fadeInUp">
                    <div id="images">
    <?php
                $conexion = mysqli_connect('localhost', 'admin', '', 'icyou');
                        if (isset($_SESSION['usuLogin']) && isset($_SESSION['usutipo'])) {


                            if ($_SESSION['usutipo'] == "fotografo") {
                                $conexion = mysqli_connect('localhost', 'fotografo', '', 'icyou');
                                if (mysqli_connect_errno()) {
                                    printf("Conexión fallida %s\n", mysqli_connect_error());
                                    exit();
                                }//codigo para buscar fotos segun sus hashtags, los sigas o no
                                $user=$_SESSION['usuLogin'];
                                if(isset($_POST['seguir'])){
                                    $eti=$_POST['etiquetaIntro'];
                                    $sqlComprobacion="SELECT * FROM seguimientos WHERE usuario='$user' AND etiqueta='$eti';";
                                    $resulComprobacion=mysqli_query($conexion,$sqlComprobacion);
                                    if(mysqli_num_rows($resulComprobacion)>0){
                                       ?>
                                       <span>Ya sigues ese hashtag!</span>
                                       <?php
                               
                                    }else{
                                    $sqlFollow="INSERT INTO seguimientos (usuario,etiqueta) VALUES ('$user','$eti');";
                                    $resultadoFollow = mysqli_query($conexion, $sqlFollow);
                                    }
                                }
                                if(isset($_POST['dseguir'])){
                                    $eti=$_POST['etiquetaIntro'];
                                    $sqlUnfollow="DELETE FROM seguimientos WHERE usuario='$user' AND etiqueta='$eti';";
                                    $resultadoUnfollow = mysqli_query($conexion, $sqlUnfollow);
                                }
                                if(isset($_POST['buscar'])){
                                    $eti=$_POST['etiquetaIntro'];
                                $sql = "SELECT * FROM fotos,etiquetados WHERE fotos.idFoto=etiquetados.idFoto  AND etiquetados.etiqueta='$eti'  ORDER BY fotos.fecha DESC;";
                                $resultado = mysqli_query($conexion, $sql);
                                if (mysqli_num_rows($resultado) > 0) {
                                    while ($registro = mysqli_fetch_row($resultado)) {
                        ?>
                                        <div  class="image">
                                            <a href="fotoDetalle.php?idFoto=<?php echo $registro[0]; ?>"><img class="img-fluid" width="350px" height="auto" src="<?php echo $registro[3]; ?>" /><a />
                                        </div>

                        <?php
                                        }
                                    }
                                }else{ //codigo para ver las fotos del parametro seleccionado
                                    $sql2 = "SELECT * FROM fotos WHERE pelicula='$peli' ORDER BY fecha DESC;";
                                    $resultado2 = mysqli_query($conexion, $sql2);
                                    if (mysqli_num_rows($resultado2) > 0) {
                                        while ($registro = mysqli_fetch_row($resultado2)) {

                                    ?>
                                        <div  class="image">
                                            <a href="fotoDetalle.php?idFoto=<?php echo $registro[0]; ?>"><img class="img-fluid" width="350px" height="auto" src="<?php echo $registro[3]; ?>" /><a />
                                        </div>
                                    <?php
                                        }
                                    }else{
                                        $noFollowing="Todavía no hay fotos con ese hashtag";
                                        ?>
                                        
                                      
                              

                </div>
            </div>
            <div class="d-flex justify-content-center">
                <span id="errorHash"><?php echo $noFollowing?></span>
            </div>
        </div>                   
    </section>
        <!--fin contenido-->
    </div>
    <?php
                                    }
                                }
                            }
                        }
                        ?>
    <!--fin contenedor general-->