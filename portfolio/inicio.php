<?php
session_start();

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/png2" href="../imagenes/logo3.png" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/9e71a52480.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/cd59b299f5.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />



    <title>Inicio @<?php echo $_SESSION['usuLogin'] ?></title>
    <style>
        .navbar-brand {
            font-family: 'Montserrat', sans-serif;
            font-size: 40px;
            font-weight: bold;

        }

        .navbar {
            background-color: black;
            text-transform: uppercase;

        }

        body {
            background: #f6f5f7;
        }


        .heading {
            padding: 10px 10px;
            border-radius: 2px;
            color: grey;
            margin-bottom: 10px;
            text-transform: uppercase;
            font-size: 0.75rem;
            line-height: 1.85583232;
            -webkit-column-span: all;
            column-span: all;
            justify-content: center;


        }

        .heading span {
            margin-right: 10px;
        }

        .heading span:hover {
            color: black;
            cursor: pointer;


        }

        #galeria {
            text-align: center;
            margin: 1rem auto;
            width: 100%;
            max-width: 990px;
            column-count: 3;
            break-inside: avoid;
            page-break-inside: avoid;

        }

        div .image img {
            margin: .2em;
        }

        #btnAnalog {
            background-color: black;
      border: 1px black;
       
        }

        #btnDigit {
        
            background-color: black;
            border: 1px black;
            
                    }

        #btnInstant {
        
            background-color: black;
            border: 1px black;
        }

       
        .material-icons{
          font-size:36px;
          
        }
        .navegacion{
            text-transform:uppercase;
           
        }
        .navegacion a{
            text-decoration:none;
            color:grey;
            padding:10px 10px;
            font-size: 0.9rem;
            line-height: 1.85583232;
            margin-bottom:50px;
            
        }
        .navegacion a:hover{
            text-decoration:none;
            color:black;
            
        }
        
        
     #footer{
         background:black;
     }
     
     .modal input{
        background-color: #f0f0f0;
        color: #606060;
        text-transform: uppercase;
        letter-spacing: 0.1rem;
        border-radius: 12px;
        padding: 12px 16px 10px;
        font-weight: 600;
        font-size: 0.625rem;
        width:30em;
                 
     }
    
     .modal h4{
        text-transform:uppercase;

     }
     #env{
         background-color:black;
         color:white;
     }
     #danger{
        text-transform:uppercase;
        letter-spacing: 0.1rem;
        border-radius: 12px;
        font-weight: 600;
        font-size: 0.625rem;

     }
     @media  ( max-width: 360px ) {
         #galeria{
             columns:1;
         }
     }
     @media  ( max-width: 425px ) {
         #galeria{
             columns:1;
         }
     }
    
     @media (max-width: 767px) { 
    #galeria {
        columns:2;
    }

}
        

@media (max-width: 480px) {
    #galeria {
        columns: 1;
    }
}
    </style>
</head>

<body>
    <?php 
if(isset($logout)){
    session_destroy();
   
}

?>
    <!--contenedor general-->
    <div class="container-fluid">
        <!--navegación-->
        <nav class="navbar navbar-expand-lg navbar-dark  shadow fixed-top">
            <div class="container">
                <a class="navbar-brand" href="inicio.php">
                    <img src="../imagenes/logo3.png" width="55" height="55" class="d-inline-block align-top" alt=""> ICYOU
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="inicio.php">Inicio
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="explorePage.php">Explorar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="userPerfil.php">Perfil</a>
                        </li>
                        <li class="nav-item">
                            <a name="logout" class="nav-link" href="../index.php">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>

        </nav>
        <!--fin navegación-->


        <!-- Page Content -->
        <section class="py-5 mt-5">
        <?php
        if(isset($_GET['message'])){
    $mensaje=$_GET['message'];
    echo "
    <script src='https://cdn.jsdelivr.net/npm/sweetalert2@9'></script>
    <script src='sweetalert2.all.min.js'></script>
    <script>
    Swal.fire({
        icon: 'success',
        title: '¡Listo!',
        text: '$mensaje',
        confirmButtonColor:'black',
     
      })
    </script>";

}
if(isset($_GET['messageError'])){
    $mensaje=$_GET['messageError'];
    echo "
    <script src='https://cdn.jsdelivr.net/npm/sweetalert2@9'></script>
    <script src='sweetalert2.all.min.js'></script>
    <script>
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: '$mensaje',
        confirmButtonColor:'black',
     
      })
    </script>";

}
?>
            <div class="container">

                <hr>
                <div id="galeria" class="animate__animated animate__fadeInUp">
                    <!--galeria de imagenes de la bd-->
                    <nav class="heading">
                        <span id="analogs">Analógicas</span>
                        <span id="digits">Digitales</span>
                        <span id="inst">Instantáneas</span>
                    </nav>
                    <div id="images">
                    <?php
                    $conexion = mysqli_connect('localhost', 'admin', '', 'icyou');
                    if (isset($_SESSION['usuLogin']) && isset($_SESSION['usutipo'])) {


                        if ($_SESSION['usutipo'] == "fotografo") {
                            $conexion = mysqli_connect('localhost', 'fotografo', '', 'icyou');
                            if (mysqli_connect_errno()) {
                                printf("Conexión fallida %s\n", mysqli_connect_error());
                                exit();
                            }
                            $sql = "SELECT ruta,fecha,idFoto FROM fotos ORDER BY fecha DESC";
                            $resultado = mysqli_query($conexion, $sql);
                            if (mysqli_num_rows($resultado) > 0) {
                                while ($registro = mysqli_fetch_row($resultado)) {
                    ?>
                                    <div  class="image">
                                        <!--mostramos las fotos de la BD sin filtros-->
                                        <a href="fotoDetalle.php?idFoto=<?php echo $registro[2]; ?>"><img class="img-fluid" width="350px" height="auto" src="<?php echo $registro[0]; ?>" /><a />


                                    </div>



                    <?php
                                }
                            }
                        }
                    }
                    ?>
                </div>
             
            </div>
            <div class="navegacion d-flex justify-content-center">
                <a href="#" class="prev">Anterior</a>
                <a href="#" class="next">Siguiente</a>
                </div>
         </div>

        </section>
        <!--fin del muro de fotos-->

       
      
            <!-- modal fotos analógicas -->
            <div class="modal fade" id="modalAnalog">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- cabecera -->
                        <div class="modal-header">
                            <h4 class="modal-title">Subir una foto analógica</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- cuerpo -->
                        <div class="modal-body d-flex justify-content-center">
                            <form id="formuAnalog" action="upload.php" method="post" enctype="multipart/form-data" name="inscripcion">
                                <input type="file" name="archivo"><br><br>
                                <input type="text" required name="camara" placeholder="Cámara"><br><br>
                                <input type="text" required name="objetivo" placeholder="Objetivo"><br><br>
                                <input type="text" required name="pelicula" placeholder="Película"><br><br>
                                <input type="hidden" name="categoria" value='analog'>
                                <input type="submit" id="env" value="Enviar" class="trig">


                            </form>
                        </div>

                        <!-- pie -->
                        <div class="modal-footer">
                            <button type="button" id="danger" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>
            <!--fin modal fotos analogicas-->

  <!-- modal fotos digitales -->
  <div class="modal fade" id="modalDigit">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- cabecera -->
                        <div class="modal-header">
                            <h4 class="modal-title">Subir una foto digital</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- cuerpo -->
                        <div class="modal-body d-flex justify-content-center">
                            <form id="formuAnalog" action="upload.php" method="post" enctype="multipart/form-data" name="inscripcion">
                                <input type="file" name="archivo"><br><br>
                                <input type="text" required name="objetivo" placeholder="objetivo"><br><br>
                                <input type="text" required name="camara" placeholder="cámara"><br><br>
                                <input type="hidden" name="categoria" value='digital'>
                                <input type="submit" id="env" value="Enviar" class="trig">


                            </form>
                        </div>

                        <!-- pie -->
                        <div class="modal-footer">
                            <button type="button"  id="danger" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>
            <!--fin modal fotos digitales-->

              <!-- modal fotos isntantaneas -->
              <div class="modal fade" id="modalInstant">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- cabecera -->
                        <div class="modal-header">
                            <h4 class="modal-title">Subir una foto instantánea</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- cuerpo -->
                        <div class="modal-body d-flex justify-content-center">
                            <form id="formuAnalog" action="upload.php" method="post" enctype="multipart/form-data" name="inscripcion">
                                <input type="file" name="archivo"><br><br>
                                <input type="text" required name="objetivo" placeholder="objetivo"><br><br>
                                <input type="text" required name="camara" placeholder="cámara"><br><br>
                                <input type="text" required name="papel" placeholder="papel">
                                <input type="hidden" name="categoria" value='instant'><br><br>
                                <input type="submit" id="env" value="Enviar" class="trig">


                            </form>
                        </div>

                        <!-- pie -->
                        <div class="modal-footer">
                            <button type="button"  id="danger" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>
            <!--fin modal fotos analogicas-->

        </div>
        <!--fin columna botones-->
        <div id="footer" class="row footer flex-sm-column flex-md-row  mt-2 p-2  fixed-bottom">
            <div class="col d-flex justify-content-center">
            <button type="button" id="btnAnalog" class="btn btn-primary mr-3" data-toggle="modal" data-toggle="tooltip" title="Foto analógica" data-placement="right" data-target="#modalAnalog">
                <i class="material-icons">camera_roll</i>
            </button>
          
            <button type="button" id="btnDigit" class="btn btn-primary mr-3" data-toggle="modal" data-toggle="tooltip" title="Foto digital" data-placement="right" data-target="#modalDigit">
            <i class="material-icons">add_a_photo</i>
            </button>

          
            <button type="button" id="btnInstant" class="btn btn-primary" data-toggle="modal" data-toggle="tooltip" title="Foto instantánea" data-placement="right" data-target="#modalInstant">
                <i class="material-icons">crop_original</i>
            </button>

            </div>
    </div>
    <!--fin contenedor general-->
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    <script>
    $(document).ready(function(){
        $('#analogs').click(function(){
            $('#images').load("orderPhoto/analogicas.php");
            $('#analogs').css('color','black');

            $('#digits').css('color','grey');
            $('#inst').css('color','grey');



        });
        $('#digits').click(function(){
            $('#images').load("orderPhoto/digitales.php");
            $('#digits').css('color','black');

            $('#analogs').css('color','grey');
            $('#inst').css('color','grey');



        });
        $('#inst').click(function(){
            $('#images').load("orderPhoto/instantaneas.php");
            $('#inst').css('color','black');

            $('#digits').css('color','grey');
            $('#analogs').css('color','grey');


        });
    });
    </script>
    <script>
    var start = 0;
    var nb = 10;
    var end = start + nb;
    var length = $('.image img').length;
    var list = $('.image img');

    list.hide().filter(':lt('+(end)+')').show();
    $('.prev, .next').click(function(e){
    e.preventDefault();

    if( $(this).hasClass('prev') ){
        start -= nb;
    } else {
        start += nb;
    }
    if( start < 0 || start >= length ) start = 0;
    end = start + nb;       
    if( start == 0 ) list.hide().filter(':lt('+(end)+')').show();
    else list.hide().filter(':lt('+(end)+'):gt('+(start-1)+')').show();
    });
    </script>
<?php
mysqli_close($conexion);
?>
</body>

</html>