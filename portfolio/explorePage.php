<?php
session_start();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png2" href="../imagenes/logo3.png" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">    <script src="https://kit.fontawesome.com/9e71a52480.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
  <script src="funciones.js"></script>




    <title>ICYOU - Explorar @<?php echo $_SESSION['usuLogin']?></title>

    <style>
        .navbar-brand {
            font-family: 'Montserrat', sans-serif;
            font-size: 40px;
            font-weight: bold;

        }
        .navbar {
            background-color: black;
            text-transform: uppercase;

        }
        body{
            background: #f6f5f7;
        }
        .material-icons:hover{
            cursor:pointer;
        
        }
        .material-icons{
            font-size:36px;
        }
        #tags-input{
                border-radius: 12px;
                margin: 0 0 4px 0;
                padding: 7px 12px;
                border: 1px solid #d9d9d9;
                white-space: pre-wrap;
                overflow-wrap: break-word;
                resize: none;
                width:20em;

        }
        #desc{
            text-transform: uppercase;
                letter-spacing: 0.1rem;
                font-weight: 600;
                font-size: 0.70rem;
        }
        #galeria {
            text-align: center;
            margin: 1rem auto;
            width: 100%;
            max-width: 960px;
            column-count: 3;
            break-inside: avoid;
            page-break-inside: avoid;

        }

        div .image img {
            margin: .2em;
        }
        .btn{
            border:0px;
            background: #f6f5f7;
        }
        .follow{
            text-transform: uppercase;
                letter-spacing: 0.1rem;
                font-weight: 400;
                font-size: 0.70rem;
                border:0px;
                background: #f6f5f7;
        }
        #errorHash{
            text-transform: uppercase;
                letter-spacing: 0.1rem;
                font-weight: 400;
                font-size: 0.90rem;
          
        }
        @media  ( max-width: 360px ) {
         #galeria{
             columns:1;
         }
     }
     @media  ( max-width: 425px ) {
         #galeria{
             columns:1;
         }
     }
    
     @media (max-width: 767px) { 
    #galeria {
        columns:2;
    }

}
        

@media (max-width: 480px) {
    #galeria {
        columns: 1;
    }
}

        </style>
</head>
<body onload="nobackbutton();">
<?php 
if(isset($logout)){
    session_destroy();
   
}
if(isset($_GET['tag'])){
    include 'explorePage/exploreByTag.php';

}else if(isset($_GET['cam'])){
    include 'explorePage/exploreByCam.php';
}else if(isset($_GET['obj'])){
    include 'explorePage/exploreByObj.php';

}else if(isset($_GET['pel'])){
    include 'explorePage/exploreByPeli.php';

}else if(isset($_GET['pap'])){
    include 'explorePage/exploreByPapel.php';
}else{
   include 'explorePage/exploreDefault.php';

}

?>

</body>


</html>