<?php
    session_start();
    $id = $_GET['idFoto'];

    ?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="shortcut icon" type="image/png2" href="../imagenes/logo3.png" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/9e71a52480.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script src="funciones.js"></script>

        <title>Foto #<?php echo $id ?> - @<?php echo $_SESSION['usuLogin'] ?></title>
        <style>
            .navbar-brand {
                font-family: 'Montserrat', sans-serif;
                font-size: 40px;
                font-weight: bold;

            }

            .navbar {
                background-color: black;
                text-transform: uppercase;

            }

            body {
                background: #f6f5f7;
            }

            dl {
                display: block;
                font-size: 0.90em;

            }

            dl dt {
                display: inline;
                text-transform: uppercase;

            }

            dl dd {
                display: inline;
                margin-right: 0.6em;
            }

            .meta {
                margin-top: 0.2rem;


            }

            textarea {
                border-radius: 12px;
                margin: 0 0 4px 0;
                padding: 7px 12px;
                border: 1px solid #d9d9d9;
                white-space: pre-wrap;
                overflow-wrap: break-word;
                resize: none;

            }

            .btn, .borrar {
                background-color: #f0f0f0;
                color: #606060;
                text-transform: uppercase;
                letter-spacing: 0.1rem;
                border: none;
                border-radius: 12px;
                margin: 0 0 4px 0;
                padding: 12px 16px 10px;
                font-weight: 600;
                font-size: 0.625rem;
                margin-bottom:7em;
                 
                
            }
            
            #comentarios{
                width:800px;
                margin:0 auto;
            }
       
        #fotodetallada{
            max-width:70em;
            min-width:70em;
        }
            li{
            list-style: none;
            margin-bottom:1em;
        }
        .imag img {
            width: 100%;
            float: left;
            border-radius: 99%;
        }
        .imag{
            width: 7em;
            height: 7em;
       

        }
        .dos{
            margin-left: 20px;
        }
            .nocoments{
                text-transform: uppercase;
                letter-spacing: 0.1rem;
                font-weight: 600;
                font-size: 0.725rem;
           
            }
           .usucoment, .fech{
               font-size:1rem;
               text-transform:uppercase;
               color:grey;
               font-weight:600;
           }
            a:hover{
                text-decoration:none;
            }
          .comen{
            font-size: 0.95rem;
            line-height: 1.79;
          }
         a.meta_tag:before{
             content:'#';
             color:#bfbfbf;
             
         }
       
         a.usucoment:after{
             content:'·';
             margin-left:3px;
          
         }
         dl a{
             display:inline;
         }
         .modal{
             height:20em;
         }
         .modal input{
        background-color: #f0f0f0;
        color: #606060;
        text-transform: uppercase;
        letter-spacing: 0.1rem;
        border-radius: 12px;
        padding: 12px 16px 10px;
        font-weight: 600;
        font-size: 0.625rem;
        width:30em;
                 
     }
    
     .modal h4{
        text-transform:uppercase;

     }
     .modal-body{
         margin-left:75px;
     }
     #env{
         background-color:black;
         color:white;
     }
     #danger{
        text-transform:uppercase;
        letter-spacing: 0.1rem;
        border-radius: 12px;
        font-weight: 600;
        font-size: 0.625rem;
        background-color:#dc3545;
        color:white;

     }
        </style>
    </head>
    <?php 
if(isset($logout)){
    session_destroy();
   
}

?>
    <body onload="nobackbutton();">
        <div class="container-fluid">
            <!--navegación-->
            <nav class="navbar navbar-expand-lg navbar-dark  shadow fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="inicio.php">
                        <img src="../imagenes/logo3.png" width="55" height="55" class="d-inline-block align-top" alt=""> ICYOU
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="inicio.php">Inicio
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="explorePage.php">Explorar</></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="userPerfil.php">Perfil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../index.php">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </nav>
            <!--fin navegación-->
            <?php
            $conexion = mysqli_connect('localhost', 'admin', '', 'icyou');
            if (isset($_SESSION['usuLogin']) && isset($_SESSION['usutipo'])) {


                if ($_SESSION['usutipo'] == "fotografo") {
                    $conexion = mysqli_connect('localhost', 'fotografo', '', 'icyou');
                    if (mysqli_connect_errno()) {
                        printf("Conexión fallida %s\n", mysqli_connect_error());
                        exit();
                    }
                    $sql = "SELECT * FROM fotos WHERE idFoto=$id";
                    $resultado = mysqli_query($conexion, $sql);
                    if (mysqli_num_rows($resultado) > 0) {
                        while ($registro = mysqli_fetch_row($resultado)) {
                            $fechaOrig=$registro[1];
                            $nuevaFecha=date("d-m-Y",strtotime("$fechaOrig"));
                          
            ?>
                            <section class="py-5 mt-5">
                                <div class="bloque">
                                    <div id="contFoto" class="image d-flex justify-content-center">
                                        <!--mostramos la foto en detalle-->
                                        <img  id="fotodetallada" class="img-fluid"  src="<?php echo $registro[3]; ?>" />
                                    </div>
                                    <!--hashtags-->
                                   
                                            
                                 
                                    <!--final consulta de hashtags-->
                                    <!--datos de interés de la foto-->
                                    <?php
                                          if($registro[4]=='digital'){
                                    ?>
                                    <div class="meta d-flex justify-content-center ">
                                        <dl class="dl-horizontal">
                                            <dt>Fotógrafo:</dt>
                                            <dd><a href="userPerfilExterno.php?usuario=<?php echo $registro[2] ?>"><?php echo $registro[2] ?></a></dd>
                                           <dt>Etiquetas:</dt>
                                           <dd>
                                            <?php
                                    $sqlHash="SELECT idFoto, etiqueta FROM etiquetados WHERE idFoto='$id'";
                                    $resulHash=mysqli_query($conexion,$sqlHash);
                                    if(mysqli_num_rows($resulHash)>0){
                                        while($regHash=mysqli_fetch_row($resulHash)){
                                            ?>
                                           <a class="meta_tag" href="explorePage.php?tag=<?php echo $regHash[1]?>"><?php echo $regHash[1]?></a> 
                                        </dd>
                                            <?php
                                        }
                                    }
                                    
                                    ?>
                                            <dt>Subida:</dt>
                                            <dd><?php echo $nuevaFecha ?></dd>
                                            <dt>Cámara:</dt>
                                            <dd><a href="explorePage.php?cam=<?php echo $registro[5]?>"><?php echo $registro[5] ?></a></dd>
                                            <dt>Objetivo:</dt>
                                            <dd><a href="explorePage.php?obj=<?php echo $registro[6]?>"><?php echo $registro[6] ?></a></dd>                                           
                                
                                        </dl>
                                    </div>
                                    <?php
                                          }else if($registro[4]=='analog'){
                                    ?>
                                     <div class="meta d-flex justify-content-center ">
                                        <dl class="dl-horizontal">
                                            <dt>Fotógrafo:</dt>
                                            <dd><a href="userPerfilExterno.php?usuario=<?php echo $registro[2] ?>"><?php echo $registro[2] ?></a></dd>
                                            <dt>Etiquetas:</dt>
                                           <dd>
                                            <?php
                                    $sqlHash="SELECT idFoto, etiqueta FROM etiquetados WHERE idFoto='$id'";
                                    $resulHash=mysqli_query($conexion,$sqlHash);
                                    if(mysqli_num_rows($resulHash)>0){
                                        while($regHash=mysqli_fetch_row($resulHash)){
                                            ?>
                                           <a class="meta_tag" href="explorePage.php?tag=<?php echo $regHash[1]?>"><?php echo $regHash[1]?></a> 
                                        </dd>
                                            <?php
                                        }
                                    }
                                    
                                    ?>
                                            <dt>Subida:</dt>
                                            <dd><?php echo $nuevaFecha ?></dd>
                                            <dt>Cámara:</dt>
                                            <dd><a href="explorePage.php?cam=<?php echo $registro[5]?>"><?php echo $registro[5] ?></a></dd>                                           
                                             <dt>Objetivo:</dt>
                                             <dd><a href="explorePage.php?obj=<?php echo $registro[6]?>"><?php echo $registro[6] ?></a></dd>                                           
                                            <dt>Película:</dt>
                                            <dd><a href="explorePage.php?pel=<?php echo $registro[8] ?>"><?php echo $registro[8] ?></a></dd>
                                        </dl>
                                    </div>
                                    <?php
                                          }else if($registro[4]=='instant'){

                                          
                                     ?>
                                      <div class="meta d-flex justify-content-center ">
                                        <dl class="dl-horizontal">
                                            <dt>Fotógrafo:</dt>
                                            <dd><a href="userPerfilExterno.php?usuario=<?php echo $registro[2] ?>"><?php echo $registro[2] ?></a></dd>
                                            <dt>Etiquetas:</dt>
                                           <dd>
                                            <?php
                                    $sqlHash="SELECT idFoto, etiqueta FROM etiquetados WHERE idFoto='$id'";
                                    $resulHash=mysqli_query($conexion,$sqlHash);
                                    if(mysqli_num_rows($resulHash)>0){
                                        while($regHash=mysqli_fetch_row($resulHash)){
                                            ?>
                                           <a class="meta_tag" href="explorePage.php?tag=<?php echo $regHash[1]?>"><?php echo $regHash[1]?></a> 
                                        </dd>
                                            <?php
                                        }
                                    }
                                    
                                    ?>
                                            <dt>Subida:</dt>
                                            <dd><?php echo $nuevaFecha ?></dd>
                                            <dt>Cámara:</dt>
                                            <dd><a href="explorePage.php?cam=<?php echo $registro[5]?>"><?php echo $registro[5] ?></a></dd>                                            
                                            <dt>Objetivo:</dt>
                                            <dd><a href="explorePage.php?obj=<?php echo $registro[6]?>"><?php echo $registro[6] ?></a></dd>                                           
                                            <dt>Papel:</dt>
                                            <dd><a href="explorePage.php?pap=<?php echo $registro[7]?>"><?php echo $registro[7] ?></a></dd>                                           
                                        </dl>
                                    </div>
                                    <?php 
                                          }
                                          ?>
                                    <!--fin de datos-->
                                </div>

                                <!--seccion de comentarios-->
                                <div id="comSection" class="d-flex justify-content-center">
                                    <form action="" method="POST" name="formuComentarios">
                                        <textarea cols="100" rows="3" name="comentario"></textarea><br><br>
                                        <button  class="btn" name="comentar">dejar un comentario</button>
                                       
                                        
                                        <?php 
                                        //si el usuario logueado es iugal al usuario que hizo la foto
                                        if($_SESSION['usuLogin']==$registro[2]){ 
                                        
                                       ?>
                                       <!--codigo para añadir hashtags a las fotos que pertenecen al usuario logueado-->
                                        <button type="button" class="btn" data-toggle="modal" data-target="#addTag">nuevo hashtag</button>
                                        <button  class="btn" name="borrar"><i class="far fa-trash-alt"></i></button>
                                        </form>

                                            <div class="modal fade" id="addTag">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- cabecera -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Añade un nuevo hashtag</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>

                                                        <!-- cuerpo -->
                                                        <div class="modal-body text-align-center">
                                                            <form id="formuAnalog" action="" method="post" enctype="multipart/form-data" name="formTag">
                                                                <input type="text"  name="hashtag"/><br><br>
                                                                <input type="submit" id="env" value="Enviar" name="envTag" class="trig"/>
                                                            </form>
                                                        </div>
                                                        <!-- pie -->
                                                        <div class="modal-footer">
                                                            <button type="button" id="danger" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                        if(isset($_POST['borrar'])){
                                            $sqlDel="DELETE FROM fotos WHERE idFoto='$id';";
                                            if(mysqli_query($conexion, $sqlDel)){
                                                echo "
                                                <script src='https://cdn.jsdelivr.net/npm/sweetalert2@9'></script>
                                                <script src='sweetalert2.all.min.js'></script>
                                                <script>
                                                Swal.fire({
                                                    icon: 'success',
                                                    title: 'Hecho,',
                                                    text: 'la foto ha sido eliminada',
                                                    confirmButtonColor:'black',
                                                 
                                                  })
                                                </script>";
                                          
                                            }
                                          
                                        
                                        }
                                         if(isset($_POST['envTag'])){
                                            $etiq=$_POST['hashtag'];
                                            if($etiq==""){
                                                echo "
                                                <script src='https://cdn.jsdelivr.net/npm/sweetalert2@9'></script>
                                                <script src='sweetalert2.all.min.js'></script>
                                                <script>
                                                Swal.fire({
                                                    icon: 'error',
                                                    title: 'Espera,',
                                                    text: 'No puedes crear hashtags en blanco...',
                                                    confirmButtonColor:'black',
                                                 
                                                  })
                                                </script>";
                                            }else{
                                                $sqlComprobacion="SELECT * FROM hashtags WHERE etiqueta='$etiq';";
                                                $sqlInserTag="INSERT INTO hashtags(etiqueta) VALUES ('$etiq');";
                                                $resInsert=(mysqli_query($conexion,$sqlInserTag));
                                                $res=mysqli_query($conexion,$sqlComprobacion);
                                                    if(mysqli_num_rows($res)<0){
                                                        $sqlInserTag="INSERT INTO hashtags(etiqueta) VALUES ('$etiq');";
                                                        $resInsert=(mysqli_query($conexion,$sqlInserTag));
                                                    
                                                    }else{
                                                    
                                                        $sqlInserEtiquetados="INSERT INTO etiquetados(idFoto,etiqueta) VALUES ('$id','$etiq');";
                                                        $resEtiquetados=(mysqli_query($conexion,$sqlInserEtiquetados));
                                            }
                                        }//fin si se pulsa enviar tag
                                    }
                                }//fin acciones del usuario propietario de la foto
                                         if (isset($_POST['comentar'])) {//si se pulsa comentar
                                            $coment = $_POST['comentario'];
                                            $dataTime = date("Y-m-d H:i:s");
                                            $user = $_SESSION['usuLogin'];
                                            $sqlComentar = "INSERT INTO comentarios (idComentario,fecha,descripcion,idUser,idFoto) VALUES ('','$dataTime','$coment','$user','$registro[0]');";
                                            $resComent=mysqli_query($conexion, $sqlComentar);
                                           
                                        
                                    }//fin si se pulsa comentar
                                    
                                 
                                         
                                        ?>
                                    

                                </div>
                       
                        <?php
                           
                        }
                    }
                        ?>
                        <!--seccion comentarios-->
                        <div id="comentarios">
                            <ul>
                                <?php
                                $sqlComents = "SELECT fecha,descripcion,idUser,idFoto,login,fotoPerfil FROM comentarios,usuarios WHERE idFoto=$id AND usuarios.login=comentarios.idUser ORDER BY comentarios.fecha DESC";
                                $resulComents = mysqli_query($conexion, $sqlComents);
                                if (mysqli_num_rows($resulComents) > 0) {
                                    while ($registroComents = mysqli_fetch_row($resulComents)) {
                                        $fechaOrig2=$registroComents[0];
                                        $nuevaFecha2=date("d-m-Y",strtotime("$fechaOrig2"));

                                ?>
                                        <li>
                                            <div class="uno container-fluid">
                                                <div class="row">
                                                    <div class="row imag">
                                                    <img class="profPic" src="<?php echo $registroComents[5]?>">
                                                    </div>
                                            <div class="dos col">
                                            <small>
                                                <a class="usucoment" href="userPerfilExterno.php?usuario=<?php echo $registroComents[2] ?>"><?php echo $registroComents[2] ?></a> 
                                                <span class="fech"><?php echo $nuevaFecha2 ?></span>
                                                <p class="comen"><?php echo $registroComents[1] ?></p>
                                            </small>
                                            </div>

                                                 </div>
                                             </div>
                                        </li>

                                <?php
                                    }//fin mostrar comentarios
                                } else {
                                 ?>
                                  <div class="d-flex justify-content-center">
                                    <span class="nocoments">No hay ningún comentario</span>
                                 </div>  
                                 <?php
                                }
                                ?>

                            </ul>
                        </div>
                        <!-- fin zona de comentarios-->
                 </section>


                    <?php
                }//fin de las acciones del usuario fotógrafo
            }
            mysqli_close($conexion);
                    ?>
        </div>
        <!--fin contenedor general-->

    </body>

    </html>