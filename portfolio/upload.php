<?php 
session_start();

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ICYOU - Subir una foto</title>
</head>

<body>
<?php
            $dataTime = date("Y-m-d H:i:s");
            $usuario = $_SESSION['usuLogin'];
            $carpetaDestino = "../userPhotos/$usuario/";

            $objetivo=$_POST['objetivo'];
            $categoria=$_POST['categoria'];
            $cam=$_POST['camara'];

           
            $conexion = mysqli_connect('localhost', 'fotografo', '', 'icyou');
            if (mysqli_connect_errno()) {
                printf("Conexión fallida %s\n", mysqli_connect_error());
                exit();
            }

            # si hay algun archivo que subir
            if (isset($_FILES["archivo"]) && $_FILES["archivo"]["name"]) {

                # si es un formato de imagen válido
                if ($_FILES["archivo"]["type"] == "image/jpeg" || $_FILES["archivo"]["type"] == "image/pjpeg" || $_FILES["archivo"]["type"] == "image/gif" || $_FILES["archivo"]["type"] == "image/png") {

                    # si exsite la carpeta o se ha creado
                    if (file_exists($carpetaDestino) || @mkdir($carpetaDestino)) {
                        $origen = $_FILES["archivo"]["tmp_name"];
                        $destino = $carpetaDestino . $_FILES["archivo"]["name"];

                        # movemos el archivo
                        if (@move_uploaded_file($origen, $destino)) {
                            
                            $imagenRuta = $carpetaDestino . basename($_FILES['archivo']['name']);
                            if($categoria=='digital'){
                                $sql = "INSERT INTO fotos(idFoto, fecha, idUser,ruta,categoria,camara,objetivo,papel,pelicula) VALUES ('', '$dataTime', '$usuario','$imagenRuta','$categoria','$cam','$objetivo',NULL,NULL)";
                                $resultado=mysqli_query($conexion,$sql);
                            }else if ($categoria=='instant'){
                                $papel=$_POST['papel'];
                                $sql2="INSERT INTO fotos (idFoto, fecha, idUser,ruta,categoria,camara,objetivo,papel,pelicula) VALUES ('', '$dataTime', '$usuario','$imagenRuta','$categoria','$cam','$objetivo','$papel',NULL)";
                                $resultado=mysqli_query($conexion,$sql2);
                            }else if($categoria=='analog'){
                                $pelicula=$_POST['pelicula'];
                                $sql3="INSERT INTO fotos (idFoto, fecha, idUser,ruta,categoria,camara,objetivo,papel,pelicula) VALUES ('', '$dataTime', '$usuario','$imagenRuta','$categoria','$cam','$objetivo',NULL,'$pelicula')";
                                $resultado = mysqli_query($conexion, $sql3);

                            }
                         
                            $mensaje= $_FILES["archivo"]["name"] . " subido correctamente";
                            header("Location:inicio.php?message=$mensaje");      
                        } else {
                            echo "<br>No se ha podido mover el archivo: " . $_FILES["archivo"]["name"];
                        }
                    } else {
                        echo "<br>No se ha podido crear la carpeta: " . $carpetaDestino;
                    }
                } else {
                    $mensajeError= $_FILES["archivo"]["name"] . " - NO es imagen jpg, png o gif";
                    header("Location:inicio.php?messageError=$mensajeError");      
                    
                }
            } else {
                echo "<br>No se ha subido ninguna imagen";
            }
        

   
        mysqli_close($conexion);

    ?>

</body>

</html>