<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ICYOU - Entra o regístrate</title>
    <link rel="stylesheet" href="portfolio/estiloLogin.css"/>
    <link rel="shortcut icon" type="image/png2" href="imagenes/logo3.png" />
    <script src='https://cdn.jsdelivr.net/npm/sweetalert2@9'></script>
    <script src='sweetalert2.all.min.js'></script>
<link rel="stylesheet" href="sweetalert2.min.css">
</head>
<body>

<h1 class="logo">ICYOU</h1>
<div class="container" id="container">
	<div class="form-container sign-up-container">
        <!--registro de usuarios-->
		<form action="" method="POST" name="regForm" onsubmit="return validar()" >
			<h1>Crea una cuenta</h1>
			<span>usa tu email para registrarte</span>
			<input type="text" name="nom" required placeholder="Nombre" minlength="3" />
            <input type="email" name="mail" required placeholder="Email" />
            <input type="text" name="usu" required placeholder="Usuario" />
            <input type="password" name="pass" id="pass" minlength="8" required placeholder="Contraseña" />
            <input type="password" name="rpass" id="rpasswd" onblur="valpass()" required placeholder="Repite la contraseña"/>
			<button name="registrar">Registrarme</button>
		</form>
	</div>
	<div class="form-container sign-in-container">
        <!--acceso de usuarios-->
		<form action="" method="POST">
			<h1>Bienvenido,</h1>
			<span>¡te echábamos de menos!</span>
			<input type="text" name="user" required placeholder="Usuario" />
			<input type="password" name="passw" required placeholder="Contraseña" />
			<button name="env">Entrar</button>
		</form>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-left">
				<h1>¡Bienvenido!</h1>
				<p>Para conectarte, ingresa tus datos</p>
				<button class="ghost" id="signIn">Entrar</button>
			</div>
			<div class="overlay-panel overlay-right">
				<h1>¿Eres nuevo por aquí?</h1>
				<p>Introduce tus datos y comienza un viaje con nosotros</p>
				<button class="ghost" id="signUp">Registrarme</button>
			</div>
		</div>
	</div>
</div>

<script>
const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
	container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
	container.classList.remove("right-panel-active");
});



</script>
<?php 
$conexion = mysqli_connect('localhost', 'acceso', '', 'icyou');
if (mysqli_connect_errno()) {
    printf("Conexión fallida %s\n", mysqli_connect_error());
    exit();
}

if (isset($_POST['env'])) {
    $usuario = $_POST['user'];
    $contraHash = hash_hmac("sha512", $_POST['passw'], "primeraweb", FALSE);
    $sql = "SELECT login,rol,password FROM usuarios WHERE login = '$usuario' AND password = '$contraHash'";
    $resultado = mysqli_query($conexion, $sql);
    $filas = mysqli_num_rows($resultado);

    if ($filas > 0) {
        while ($registro = mysqli_fetch_row($resultado)) {
            $usuLogin = $registro[0];
            $usutipo = $registro[1];
         
        }

        session_start();

        $_SESSION['usuLogin'] = "$usuLogin";
        $_SESSION['usutipo'] = "$usutipo";



        header("Location:portfolio/inicio.php");

        exit();
    } else {
      echo "
        <script src='https://cdn.jsdelivr.net/npm/sweetalert2@9'></script>
        <script src='sweetalert2.all.min.js'></script>
        <script>
        Swal.fire({
            icon: 'warning',
            title: '¿Todo bien?',
            text: 'El usuario o contraseña son incorrectos',
         
          })
        </script>";
    
    }
}

//registro de usuario

if(isset($_POST['registrar'])) {
    $usu = $_POST['usu'];
    $sqlUsuarios = "SELECT login from usuarios where login='$usu' ;";
    $conexion = mysqli_connect("localhost", "acceso", "", "icyou");

    $resulUsuarios = mysqli_query($conexion, $sqlUsuarios);
    if (mysqli_num_rows($resulUsuarios) > 0) {
        echo " <script src='https://cdn.jsdelivr.net/npm/sweetalert2@9'></script>
        <script src='sweetalert2.all.min.js'></script>
        <script>
        Swal.fire({
            icon: 'error',
            title: 'Oops!',
            text: 'Ese nombre de usuario ya está cogido...',
            confirmButtonColor:'black',
         
          })
        </script>";
    } else {

        $contra = $_POST['rpass'];
        $user = $_POST['usu'];
        $nombre = $_POST['nom'];
        $correo = $_POST['mail'];
        $contraHash = hash_hmac("sha512", $contra, "primeraweb", FALSE);

        $conexion = mysqli_connect("localhost", "acceso", "", "icyou");
        if (mysqli_connect_errno()) {
            printf("Conexión fallida %s\n", mysqli_connect_error());
            exit();
        }

        $sql = "INSERT INTO usuarios (login,nombre,email,password,rol) VALUES ('$user','$nombre','$correo','$contraHash','fotografo');";
        if (mysqli_query($conexion, $sql)) {
            echo "
            <script src='https://cdn.jsdelivr.net/npm/sweetalert2@9'></script>
            <script src='sweetalert2.all.min.js'></script>
            <script>
            Swal.fire({
                icon: 'success',
                title: '¡Listo!',
                text: 'Ya puedes iniciar sesión',
                confirmButtonColor:'black',
             
              })
            </script>";
        
          
        } else {
            echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
        }
        ?>
    
        <?php
    }
    mysqli_close($conexion);
}
?>

    <script>
function validar() {
            if (valpass()) {
                return true;
            } else {
            
  
    Swal.fire({
        icon: 'warning',
        title: '¡Cuidado!',
        text: 'Tus datos son erróneos',
        confirmButtonColor:'black',
     
      })
   
                return false;
            }
        }

        function valpass() {
            var contra = document.regForm.pass.value;
            var rcontra = document.regForm.rpass.value;

            if (contra === rcontra) {
                document.getElementById('rpasswd').style.border = "3px solid green";
                return true;
            } else {
                document.getElementById('rpasswd').style.border = "3px solid red";
                return false;
            }
        }
        </script>
     
</body>
</html>