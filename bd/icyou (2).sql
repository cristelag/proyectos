-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-06-2020 a las 13:34:34
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `icyou`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `idComentario` int(5) NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` varchar(65) COLLATE utf8mb4_spanish_ci NOT NULL,
  `idUser` varchar(15) COLLATE utf8mb4_spanish_ci NOT NULL,
  `idFoto` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`idComentario`, `fecha`, `descripcion`, `idUser`, `idFoto`) VALUES
(79, '2020-06-06', 'Bonito retrato', 'cristelag', 79),
(103, '2020-06-07', 'genial!', 'cristelag', 74),
(104, '2020-06-07', 'bonito coche!', 'cristelag', 82),
(111, '2020-06-08', 'bonita', 'marina', 79);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etiquetados`
--

CREATE TABLE `etiquetados` (
  `idFoto` int(4) NOT NULL,
  `etiqueta` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `etiquetados`
--

INSERT INTO `etiquetados` (`idFoto`, `etiqueta`) VALUES
(75, 'flores'),
(47, 'flores'),
(72, 'ciudad'),
(72, 'moderno'),
(45, 'B&W'),
(79, 'atardecer'),
(80, 'skate'),
(82, 'ByN'),
(104, 'gato'),
(104, 'dobleexposicion'),
(100, 'nature'),
(105, 'ciudad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotos`
--

CREATE TABLE `fotos` (
  `idFoto` int(4) NOT NULL,
  `fecha` date NOT NULL,
  `idUser` varchar(15) COLLATE utf8mb4_spanish_ci NOT NULL,
  `ruta` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `categoria` enum('analog','digital','instant') COLLATE utf8mb4_spanish_ci NOT NULL,
  `camara` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `objetivo` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `papel` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `pelicula` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `fotos`
--

INSERT INTO `fotos` (`idFoto`, `fecha`, `idUser`, `ruta`, `categoria`, `camara`, `objetivo`, `papel`, `pelicula`) VALUES
(42, '2020-03-15', 'cristelag', '../userPhotos/cristelag/foto2.jpg', 'instant', 'fujifilm instax mini 7', 'fujifilm', 'fujifilm instax mini film', NULL),
(43, '2020-04-12', 'huwito11', '../userPhotos/huwito11/foto3.jpg', 'instant', 'fujifilm instax mini 7', 'fujifilm', 'fujifilm instax mini', NULL),
(44, '2020-04-20', 'huwito11', '../userPhotos/huwito11/foto4.jpg', 'analog', 'Lomo LC -A+ Russia Day', 'Lomo LC-A MINITAR 1:2.8', NULL, 'Svema Foto 100'),
(45, '2020-05-14', 'huwito11', '../userPhotos/huwito11/foto5.jpg', 'analog', 'Fujifilm XT2', 'New Petzval 85 Art Lens', NULL, 'kodak gold'),
(46, '2020-02-14', 'huwito11', '../userPhotos/huwito11/foto6.jpg', 'analog', 'Yashica T4', 'Carl Zeiss T* Tessar 3,5/35', NULL, 'Kodak Ultramax 400'),
(47, '2020-02-24', 'huwito11', '../userPhotos/huwito11/foto7.jpg', 'analog', 'Fujifilm XT2', 'New Petzval 85', NULL, 'Kodak ultramax'),
(72, '2020-06-03', 'huwito11', '../userPhotos/huwito11/450_1000.jpg', 'digital', 'fghht', 'fghh', NULL, NULL),
(74, '2020-06-03', 'huwito11', '../userPhotos/huwito11/foto12.jpg', 'instant', 'adwo', 'algo', 'dlasdl', NULL),
(75, '2020-06-03', 'huwito11', '../userPhotos/huwito11/foto11.jpg', 'instant', 'adwogfgr', 'algofdg', 'dlasdlrrrr', NULL),
(76, '2020-06-03', 'huwito11', '../userPhotos/huwito11/unnamed.jpg', 'digital', 'ddfdffdff', 'dddfdfff', NULL, NULL),
(79, '2020-06-06', 'cristelag', '../userPhotos/cristelag/foto13.jpg', 'analog', 'Yashica mood', 'Yashica 470xT', NULL, 'Kodak gold'),
(80, '2020-06-06', 'cristelag', '../userPhotos/cristelag/foto14.jpg', 'analog', 'Yashica mood', 'Yashica 470xT', NULL, 'Kodak gold'),
(82, '2020-06-06', 'cristelag', '../userPhotos/cristelag/foto10.jpg', 'analog', 'Minolta ', 'Minolta S94', NULL, 'Kodak BW'),
(100, '2020-06-07', 'cristelag', '../userPhotos/cristelag/cctv-vigila.jpg', 'analog', 'Minolta', 'Minolta S94', NULL, 'Kodak Color Plus'),
(101, '2020-06-07', 'cristelag', '../userPhotos/cristelag/foto15.jpg', 'instant', 'fujifilm instax mini 7', 'Fujifilm', 'fujifilm instax mini film', NULL),
(102, '2020-06-07', 'cristelag', '../userPhotos/cristelag/foto1.jpg', 'analog', 'Yashica', 'Yashica 470xT', NULL, 'kodak gold'),
(103, '2020-06-07', 'cristelag', '../userPhotos/cristelag/foto17.jpg', 'instant', 'Fujifilm Instax Mini 7', 'Fujifilm', 'fujifilm instax mini film', NULL),
(104, '2020-06-07', 'cristelag', '../userPhotos/cristelag/foto16.jpg', 'analog', 'yashica', 'yashica S540', NULL, 'Kodak BW'),
(105, '2020-06-07', 'cristelag', '../userPhotos/cristelag/foto8.jpg', 'analog', 'Yashica mood', 'Yashica 470xT', NULL, 'Kodak gold'),
(110, '2020-06-08', 'marina', '../userPhotos/marina/cerdito.jpg', 'digital', 'ffff', 'fffffff', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hashtags`
--

CREATE TABLE `hashtags` (
  `etiqueta` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `hashtags`
--

INSERT INTO `hashtags` (`etiqueta`) VALUES
('atardecer'),
('B&W'),
('ByN'),
('cerdito'),
('cerdo'),
('circo'),
('ciudad'),
('diversion'),
('dobleexposicion'),
('edificio'),
('flores'),
('gato'),
('moderno'),
('morado'),
('naturaleza'),
('nature'),
('party'),
('rosa'),
('skate');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguimientos`
--

CREATE TABLE `seguimientos` (
  `usuario` varchar(15) COLLATE utf8mb4_spanish_ci NOT NULL,
  `etiqueta` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `seguimientos`
--

INSERT INTO `seguimientos` (`usuario`, `etiqueta`) VALUES
('huwito11', 'ciudad'),
('huwito11', 'flores'),
('cristelag', 'ciudad'),
('cristelag', 'flores'),
('marina', 'moderno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `login` varchar(15) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_spanish_ci NOT NULL,
  `password` longtext COLLATE utf8mb4_spanish_ci NOT NULL,
  `rol` enum('fotografo','administrador') COLLATE utf8mb4_spanish_ci NOT NULL,
  `fotoPerfil` longtext COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '../imagenes/default2.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`login`, `nombre`, `email`, `password`, `rol`, `fotoPerfil`) VALUES
('beabm', 'Beatriz Berrocal', 'bea@gmail.com', '3f9cbc46b36bf3837a7b8f5f4a83654f6997264acd2b5b28232e85fb148338d079c5760a57c98a057deb11ecbf4bc3dcd4ebe2c9e3f3800ebf838357d857fb12', 'fotografo', '../imagenes/default2.png'),
('carlax', 'Carla', 'carla@gmail.com', '3f9cbc46b36bf3837a7b8f5f4a83654f6997264acd2b5b28232e85fb148338d079c5760a57c98a057deb11ecbf4bc3dcd4ebe2c9e3f3800ebf838357d857fb12', 'fotografo', '../userProfPic/huwito11/cerdito.jpg'),
('cristelag', 'Cristel', 'supercristel00@gmail.com', '3f9cbc46b36bf3837a7b8f5f4a83654f6997264acd2b5b28232e85fb148338d079c5760a57c98a057deb11ecbf4bc3dcd4ebe2c9e3f3800ebf838357d857fb12', 'fotografo', '../userProfPic/cristelag/WhatsApp Image 2019-06-20 at 01.03.38.jpeg'),
('huwito11', 'hugo', 'hugo@gmail.com', '3fcc04ce24413003f840da05ff7c88d3805bc3a3f6c47044459b54f842f5197048d5f78c57f48d9f12cc39c7623f8ba2fa66ced39429d786ab8ba0882af8e19e', 'fotografo', '../userProfPic/huwito11/foto11.jpg'),
('marina', 'Marina González', 'marina@gmail.com', '3f9cbc46b36bf3837a7b8f5f4a83654f6997264acd2b5b28232e85fb148338d079c5760a57c98a057deb11ecbf4bc3dcd4ebe2c9e3f3800ebf838357d857fb12', 'fotografo', '../userProfPic/marina/foto11.jpg'),
('olayaig', 'Olaya Iglesias Garcia', 'oalta@gmail.com', 'ccfcd3726caab8bccd6fc10be5d2344712cd64ea5a6058a467f0aa7f0cf64667298d64e3ee05161175c9b19ccfcd1000117e8b6f4925c87b5a7bc789c06f3e06', 'fotografo', '../imagenes/default2.png'),
('saralv', 'Sara Alvarez Alonso', 'sara@gmail.com', 'ccfcd3726caab8bccd6fc10be5d2344712cd64ea5a6058a467f0aa7f0cf64667298d64e3ee05161175c9b19ccfcd1000117e8b6f4925c87b5a7bc789c06f3e06', 'fotografo', '../imagenes/default2.png');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idComentario`),
  ADD KEY `idFoto` (`idFoto`) USING BTREE,
  ADD KEY `idUser` (`idUser`) USING BTREE;

--
-- Indices de la tabla `etiquetados`
--
ALTER TABLE `etiquetados`
  ADD KEY `idFoto` (`idFoto`) USING BTREE,
  ADD KEY `etiqueta` (`etiqueta`) USING BTREE;

--
-- Indices de la tabla `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`idFoto`),
  ADD KEY `idUser` (`idUser`) USING BTREE;

--
-- Indices de la tabla `hashtags`
--
ALTER TABLE `hashtags`
  ADD PRIMARY KEY (`etiqueta`);

--
-- Indices de la tabla `seguimientos`
--
ALTER TABLE `seguimientos`
  ADD KEY `idUser` (`usuario`) USING BTREE,
  ADD KEY `etiqueta` (`etiqueta`) USING BTREE;

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`login`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `idComentario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT de la tabla `fotos`
--
ALTER TABLE `fotos`
  MODIFY `idFoto` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`idFoto`) REFERENCES `fotos` (`idFoto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comentarios_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `usuarios` (`login`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `etiquetados`
--
ALTER TABLE `etiquetados`
  ADD CONSTRAINT `etiquetados_ibfk_1` FOREIGN KEY (`etiqueta`) REFERENCES `hashtags` (`etiqueta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `etiquetados_ibfk_2` FOREIGN KEY (`idFoto`) REFERENCES `fotos` (`idFoto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `fotos`
--
ALTER TABLE `fotos`
  ADD CONSTRAINT `fotos_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `usuarios` (`login`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `seguimientos`
--
ALTER TABLE `seguimientos`
  ADD CONSTRAINT `seguimientos_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`login`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `seguimientos_ibfk_2` FOREIGN KEY (`etiqueta`) REFERENCES `hashtags` (`etiqueta`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
